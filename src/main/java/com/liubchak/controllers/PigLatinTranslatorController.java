package com.liubchak.controllers;

import com.liubchak.services.Translator;
import com.liubchak.validators.EnglishLanguageConstraint;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

@RestController
@Validated
public class PigLatinTranslatorController {

    final Translator translator;

    public PigLatinTranslatorController(Translator translator) {
        this.translator = translator;
    }

    @PostMapping("/translate")
    public String translate(@NotBlank @EnglishLanguageConstraint @RequestBody String text) {
           return translator.translateText(text);
    }
}
