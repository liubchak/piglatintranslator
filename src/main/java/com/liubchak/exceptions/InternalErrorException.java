package com.liubchak.exceptions;

public class InternalErrorException extends Exception{
    public InternalErrorException(String message) {
        super(message);
    }
}
