package com.liubchak.services;

import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.regex.Pattern;


@Service
public class PigLatinTranslator implements Translator {

    /**
     * This method provide pig latin translator for english language.
     * This is internal method and should be called only from controller.
     * Validation of input should be provided on controller level.
     * Method can accept next types of text: letter, symbol, sentence, or paragraph.
     * Escape characters are not supported and will be processed and treated as letters without \
     * If input contains only special characters - they will be returned without any changes
     * If first symbol is number word will be returned without any changes
     * @param text
     * @return translated text to pig latin english
     */
    @Override
    public String translateText(@NotNull String text) {
        /*
        Because this implementation is part of service, I dont want to add null check. Just warning.
        It will be done on the controller side with bean validation.
        In case if it will be a library - here should be null check and english language check and if it's not pass throw
        exception outside.
        Another option in case of null can be just return null back. It will be kinda a little bit more faster,
        but it will be not be clear why and what happened inside method, so I prefer exception.

        Code example:
        Objects.requireNonNull(text);
        if (text.matches("\\A\\p{ASCII}*\\z")) {
            throw new LanguageNotSupportedException();
        }
         */
        String[] words = text.split(" ");
        StringJoiner result = new StringJoiner(" ");
        for (String word : words) {
            result.add(translateWord(word));
        }
        return result.toString();
    }

    protected String translateWord(String word) {
        StringJoiner result = new StringJoiner("-");
        String[] subwords = word.split("-");
        if (subwords.length != 1) {
            for (String w : subwords) {
                result.add(convert(w));
            }
        } else {
            result.add(convert(word));
        }
        return result.toString();
    }


    /*
    Another possible implementation is create array with size word length + (2 | 3 depends on first letter),
    and then start moving from char array to new one with applying rules. It can be done in one iteration with a lot of if else inside
    but code will be not easy to read, and it's production ready code it's important.
    But it can depends on requirements of how fast it should translate.
     */
    protected String convert(String word) {
        if (word.endsWith("way") || "".equals(word) || Character.isDigit(word.charAt(0))) {
            return word;
        }

        char[] chars = word.toCharArray();
        StringBuilder result = new StringBuilder();

        Map<Integer, Character> punctuationMap = new HashMap<>();
        List<Integer> capitalsIndexes = new ArrayList<>();

        for (int i = 0; i < chars.length; i++) {
            char current = chars[i];
            /*
            Matchers are costly operation, so it can be changed with some hash set to store supported punctuation symbols,
            but matcher will cover more cases
             */
            boolean isPunctuation = Pattern.matches("[\\p{Punct}\\p{IsPunctuation}]", Character.toString(current));
            if (isPunctuation) {
                punctuationMap.put(chars.length - i, current);
            } else {
                if (Character.isUpperCase(current)) {
                    capitalsIndexes.add(i);
                }
                result.append(Character.toLowerCase(current));
            }

        }

        translateToPigLatin(result);
        addCapitalization(capitalsIndexes, result);
        addPunctuation(punctuationMap, result);

        return result.toString();
    }


    protected boolean isVowel(char ch) {
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }

    protected void addCapitalization(List<Integer> capitalLetterIndexes, StringBuilder word) {
        if (!capitalLetterIndexes.isEmpty()) {
            capitalLetterIndexes.forEach(i -> word.replace(i, i + 1, Character.toString(Character.toUpperCase(word.charAt(i)))));
        }
    }

    protected void addPunctuation(Map<Integer, Character> punctuationMap, StringBuilder word) {
        if (!punctuationMap.isEmpty()) {
            punctuationMap.forEach((key, value) -> word.insert(word.length() + 1 - key, value));
        }
    }

    protected void translateToPigLatin(StringBuilder word){
        if(word.length() != 0){
            if (isVowel(word.charAt(0))) {
                word.append("way");
            } else {
                word.append(word.charAt(0)).deleteCharAt(0).append("ay");
            }
        }
    }

}
