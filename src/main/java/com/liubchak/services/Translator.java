package com.liubchak.services;

public interface Translator {
    String translateText(String text);
}
