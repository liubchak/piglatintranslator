package com.liubchak.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = EnglishValidator.class)
@Target( {ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface EnglishLanguageConstraint {
    String message() default "Only english language supported. Please use english symbols";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
