package com.liubchak.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EnglishValidator implements ConstraintValidator<EnglishLanguageConstraint, String> {
    @Override
    public void initialize(EnglishLanguageConstraint constraintAnnotation) {
        //Method must be implemented by the contract, but not needed in this implementation
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value.matches("\\A\\p{ASCII}*\\z");
    }
}
