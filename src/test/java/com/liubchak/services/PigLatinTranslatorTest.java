package com.liubchak.services;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PigLatinTranslatorTest {


    private final PigLatinTranslator pigLatinTranslator = new PigLatinTranslator();

    @Test
    void translateTextWithConsonant()  {
        assertEquals("Ellohay", pigLatinTranslator.translateText("Hello"));
    }

    @Test
    void translateTextWithVowel(){
        assertEquals("appleway", pigLatinTranslator.translateText("apple"));
    }

    @Test
    void translateTextWithWayWord(){
        assertEquals("stairway", pigLatinTranslator.translateText("stairway"));
    }
    @Test
    void translateTextWithFewPunctuation(){
        assertEquals("endwa.y.", pigLatinTranslator.translateText("en.d."));
    }
    @Test
    void translateTextWithWordPunctuation(){
        assertEquals("antca'y", pigLatinTranslator.translateText("can't"));
    }
    @Test
    void translateTextWithPunctuation(){
        assertEquals("endway.", pigLatinTranslator.translateText("end."));
    }
    @Test
    void translateTextWithHyphens(){
        assertEquals("histay-hingtay", pigLatinTranslator.translateText("this-thing"));
    }
    @Test
    void translateTextWithHyphensAndPunctuation(){
        assertEquals("hista'y-hingta'y", pigLatinTranslator.translateText("thi's-thin'g"));
    }
    @Test
    void translateTextWithCapitalization(){
        assertEquals("Eachbay", pigLatinTranslator.translateText("Beach"));
    }
    @Test
    void translateTextWithFewCapitalLetter(){
        assertEquals("CcLoudmay", pigLatinTranslator.translateText("McCloud"));
    }

    @Test
    void translateTextWithCapitalLettersAndPunctuation(){
        assertEquals("CcLoud.may,", pigLatinTranslator.translateText("McCl.oud,"));

    }
    @Test
    void translateTextInSentence(){
        assertEquals("Ellohay appleway stairway endwa.y. antca'y endway. histay-hingtay Eachbay CcLoudmay CcLoud.may,",
                pigLatinTranslator.translateText("Hello apple stairway en.d. can't end. this-thing Beach McCloud McCl.oud," ));


    }
    @Test
    void translateTextInSentenceWithEscapeChars(){
        assertEquals("Ellohay appleway stairway endwa.y. antca'y endway. \nay histay-hingtay Eachbay CcLoudmay CcLoud.may,",
                pigLatinTranslator.translateText("Hello apple stairway en.d. can't end. \n this-thing Beach McCloud McCl.oud," ));

    }
    @Test
    void translateTextWithoutLetters(){
        assertEquals("..", pigLatinTranslator.translateText(".."));
    }

    @Test
    void translateTextOnlyNumbers(){
        assertEquals("8", pigLatinTranslator.translateText("8"));
    }

    @Test
    void translateTextNumbersAndLetters(){
        assertEquals("8aa", pigLatinTranslator.translateText("8aa"));
    }

    @Test
    void translateTextOnlyLettersAndNumbers(){
        assertEquals("aa8way", pigLatinTranslator.translateText("aa8"));
    }

    @Test
    void translateEmptyInput(){
        assertEquals("", pigLatinTranslator.translateText(""));
    }

    @Test
    void translateTextWithNullAsParam(){
        assertThrows(NullPointerException.class, () -> pigLatinTranslator.translateText(null));
    }

    @Test
    void isVowel() {
        assertTrue(pigLatinTranslator.isVowel('a'));
    }

    @Test
    void isConsonant() {
        assertFalse(pigLatinTranslator.isVowel('b'));
    }

}